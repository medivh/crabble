#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR/crabble.gdx
lein clean
lein jar -nomain
cd $DIR/android
ant clean debug
