(ns crabble.gdx.grid
  (:import (com.badlogic.gdx ApplicationListener Gdx Screen)
           (com.badlogic.gdx.files FileHandle)
           (com.badlogic.gdx.graphics GL20 Mesh VertexAttribute VertexAttributes$Usage)
           (com.badlogic.gdx.graphics.g2d SpriteBatch)
           (com.badlogic.gdx.graphics.glutils ShaderProgram)
           (com.badlogic.gdx.math Vector2)
           (com.badlogic.gdx.physics.box2d Body BodyDef PolygonShape World)
           (com.badlogic.gdx.scenes.scene2d Group)
           (com.badlogic.gdx.scenes.scene2d.ui Image)
           (com.badlogic.gdx.utils Array)
           (com.badlogic.gdx.utils Json)
           (java.lang IllegalStateException)
           ))

; globals
(declare ^Mesh mesh)
(declare ^ShaderProgram meshShader)

(defn create-shader []
  (let [
       ;;this shader tells opengl where to put things
       vertexShader "
attribute vec4 a_position;
void main()
{
  gl_Position = a_position;
}
",
       ;;this one tells it what goes in between the points (e.g. colour/texture)
       fragmentShader "
  #ifdef GL_ES
  precision mediump float;
  #endif
  void main()
  {
    gl_FragColor = vec4(1.0,0.0,0.0,1.0);
  }
",
       meshShader (ShaderProgram. vertexShader fragmentShader),
       ]
    (if (not (.isCompiled meshShader))
      (throw (IllegalStateException. (.getLog meshShader))))
    meshShader
    ))

(defn create-grid []
  (proxy [Screen] []
    (show []
      ;;(println (.isGL11Available (. Gdx graphics)))
      ;;(println (.isGL20Available (. Gdx graphics)))
      (let [^VertexAttribute va (VertexAttribute. VertexAttributes$Usage/Position, 3, "a_position")]
        (def mesh (Mesh. true 44 44 (into-array VertexAttribute [va]))))
      (def meshShader (create-shader))
      (doto mesh
        ;; 0.9 to -0.4
        (.setVertices (float-array [
                                    ;; vertical lines
                                    -0.9, 0.9, 0, ;0
                                     0.9, 0.9, 0,
                                    -0.9, 0.75, 0, ;1
                                     0.9, 0.75, 0,
                                    -0.9, 0.6, 0, ;2
                                     0.9, 0.6, 0,
                                    -0.9, 0.45, 0, ;3
                                     0.9, 0.45, 0,
                                    -0.9, 0.3, 0, ;4
                                     0.9, 0.3, 0,
                                    -0.9, 0.15, 0, ;5
                                     0.9, 0.15, 0,
                                    -0.9, 0.0, 0, ;6
                                     0.9, 0.0, 0,
                                    -0.9, -0.15, 0, ;7
                                     0.9, -0.15, 0,
                                    -0.9, -0.30, 0, ;8
                                     0.9, -0.30, 0,
                                    -0.9, -0.45, 0, ;9
                                     0.9, -0.45, 0,
                                    -0.9, -0.60, 0, ;10
                                     0.9, -0.60, 0,
                                    ;; horizontal lines
                                    -0.9, 0.9, 0, ;0
                                    -0.9, -0.6, 0,
                                    -0.72, 0.9, 0, ;1
                                    -0.72, -0.6, 0,
                                    -0.54, 0.9, 0, ;2
                                    -0.54, -0.6, 0,
                                    -0.36, 0.9, 0, ;3
                                    -0.36, -0.6, 0,
                                    -0.18, 0.9, 0, ;4
                                    -0.18, -0.6, 0,
                                    0.0, 0.9, 0, ;5
                                    0.0, -0.6, 0,
                                    0.18, 0.9, 0, ;6
                                    0.18, -0.6, 0,
                                    0.36, 0.9, 0, ;7
                                    0.36, -0.6, 0,
                                    0.54, 0.9, 0, ;8
                                    0.54, -0.6, 0,
                                    0.72, 0.9, 0, ;9
                                    0.72, -0.6, 0,
                                    0.9, 0.9, 0, ;10
                                    0.9, -0.6, 0,
                                    ]))
        (.setIndices (short-array [
                                   ;; horizontal
                                   (short 0), (short 1),
                                   (short 2), (short 3),
                                   (short 4), (short 5),
                                   (short 6), (short 7),
                                   (short 8), (short 9), ;4
                                   (short 10), (short 11),
                                   (short 12), (short 13),
                                   (short 14), (short 15),
                                   (short 16), (short 17),
                                   (short 18), (short 19), ;9
                                   (short 20), (short 21),
                                   ;; vertical
                                   (short 22), (short 23),
                                   (short 24), (short 25),
                                   (short 26), (short 27),
                                   (short 28), (short 29),
                                   (short 30), (short 31), ;4
                                   (short 32), (short 33),
                                   (short 34), (short 35),
                                   (short 36), (short 37),
                                   (short 38), (short 39),
                                   (short 40), (short 41), ;9
                                   (short 42), (short 43),
                                   ]))))
    (render [delta]
      ;;(.glClearColor (. Gdx gl) 1 0 0 1)
      (.glClear (. Gdx gl) (. GL20 GL_COLOR_BUFFER_BIT))
      (.begin meshShader)
      (.render mesh meshShader (. GL20 GL_LINES))
      (.end meshShader))
    (dispose[]
      (.dispose mesh)
      (.dispose meshShader))
    (hide [])
    (pause [])
    (resize [w h])
    (resume [])))


;(def gameUnitConst 32)
;
;(defn create-grid [world]
;  (let [x 100,
;        y 100,
;        width 100,
;        height 100,
;        grid (proxy [Group] []
;               (draw [batch parentAlpha]
;                 (proxy-super draw batch parentAlpha)
;                 )),
;        groundBodyDef (BodyDef.),
;        platformBody (.createBody world groundBodyDef),
;        groundBox (PolygonShape.)]
;    (.set (.position groundBodyDef) (+ x (/ width 2)) (+ y (/ height 2)))
;    (.setAsBox groundBox (/ width 2) 0.5)
;    (.createFixture platformBody groundBox 0.0)
;    ))

;        file (internal. (.files Gdx) 'data/level3.json),
;        json (Json.),
;        items (.fromJson json Array GameItem file)
