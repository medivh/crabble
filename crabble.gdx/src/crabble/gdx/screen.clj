(ns crabble.gdx.screen
  (:use crabble.gdx.grid)
  (:import (com.badlogic.gdx Gdx Screen)
           (com.badlogic.gdx.graphics Color GL10 Texture Texture$TextureFilter)
           (com.badlogic.gdx.graphics.g2d BitmapFont SpriteBatch)
           (com.badlogic.gdx.math Vector2)
;           (com.badlogic.gdx.physics.box2d World)
           (com.badlogic.gdx.scenes.scene2d Group Stage)
           (com.badlogic.gdx.scenes.scene2d.ui Label Label$LabelStyle Image)
           (com.badlogic.gdx.utils Scaling)
           (crabble.gdx.world CrabbleWorld)
           (crabble.gdx.renderer WorldRenderer)
           )
  )

; globals
;(declare ^World world)
(declare ^CrabbleWorld world)
(declare ^WorldRenderer renderer)
(declare ^Stage stage)
(declare ^SpriteBatch batch)
(declare ^Group grid)

(defn create-screen2 []
  (def world (CrabbleWorld.))
  (def renderer (WorldRenderer. world true))

  (proxy [Screen] []
    (render [delta]
      (.glClearColor (. Gdx gl) 0.1 0.1 0.1 1.0)
      (.glClear (. Gdx gl) (. GL10 GL_COLOR_BUFFER_BIT))
      (.render renderer)
      )
    (resize [w h]
      (.setSize renderer w h))
    (show [])
    (hide [])
    (dispose[])
    (pause [])
    (resume [])))

(defn create-screen []
  ; vars
;  (def world (World. (Vector2. 0 -1) true))
  (def stage (Stage.))
  (def batch (SpriteBatch.))
  ;;(def grid (create-grid world))

  (proxy [Screen] []
    (show []
      (let [font (BitmapFont.)
            style (Label$LabelStyle. font (Color. 1.0 1.0 1.0 1.0)),
            label (Label. "Hello Clojure!" style),
            texture (Texture. (.internal (. Gdx files) "resources/wood-square.jpg")),
            ]
        (.setFilter texture (. Texture$TextureFilter Linear) (. Texture$TextureFilter Linear))
        (.addActor stage label)
        (let [image (Image. texture)]
          (.setX image 100)
          (.setY image 100)
          (.addActor stage image))
        ))

    (render [delta]
      ; render red screen
      (.glClearColor (. Gdx gl) 1 0 0 1)
      (.glClear (. Gdx gl) (. GL10 GL_COLOR_BUFFER_BIT))
      ; render texture
;      (doto batch
;        (.begin)
;        (.end))
      (doto stage
        (.act delta)
        (.draw))
      )

    (dispose[])
    (hide [])
    (pause [])
    (resize [w h])
    (resume [])))
