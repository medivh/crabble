(ns crabble.gdx.core
  (:import (com.badlogic.gdx Gdx)
           (com.badlogic.gdx.backends.lwjgl LwjglApplication LwjglApplicationConfiguration)
           (com.badlogic.gdx.utils GdxNativesLoader)
           (crabble.gdx.game CrabbleGame))
  (:use crabble.gdx.triangle))

(defn App[]
  (GdxNativesLoader/load)
  (let [config (LwjglApplicationConfiguration.)]
    (set! (. config title) "Crabble!")
    (set! (. config width) 480)
    (set! (. config height) 320)
    (set! (. config useGL20) true)
    (LwjglApplication. (CrabbleGame.) config)
    ))

  ;(LwjglApplication. (create-triangle) config)
  ;(LwjglApplication. (CrabbleGame.) "Crabble!" 800 480 true))

(defn -main [& args]
  (App))
