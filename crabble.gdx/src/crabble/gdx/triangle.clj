(ns crabble.gdx.triangle
  (:import (com.badlogic.gdx ApplicationListener Gdx Screen)
           (com.badlogic.gdx.graphics GL20 Mesh VertexAttribute VertexAttributes$Usage)
           (com.badlogic.gdx.graphics.glutils ShaderProgram)
           (java.lang IllegalStateException)
           ))

; globals
(declare ^Mesh mesh)
(declare ^ShaderProgram meshShader)

(defn create-shader []
  (let [
       ;;this shader tells opengl where to put things
       vertexShader "
attribute vec4 a_position;
void main()
{
  gl_Position = a_position;
}
",
       ;;this one tells it what goes in between the points (e.g. colour/texture)
       fragmentShader "
  #ifdef GL_ES
  precision mediump float;
  #endif
  void main()
  {
    gl_FragColor = vec4(1.0,0.0,0.0,1.0);
  }
",
       meshShader (ShaderProgram. vertexShader fragmentShader),
       ]
    (if (not (.isCompiled meshShader))
      (throw (IllegalStateException. (.getLog meshShader))))
    meshShader
    ))

(defn create-triangle []
  (proxy [Screen] []
    (show []
      ;;(println (.isGL11Available (. Gdx graphics)))
      ;;(println (.isGL20Available (. Gdx graphics)))
      (let [^VertexAttribute va (VertexAttribute. VertexAttributes$Usage/Position, 3, "a_position")]
        (def mesh (Mesh. true 3 3 (into-array VertexAttribute [va]))))
      (def meshShader (create-shader))
      (doto mesh
        (.setVertices (float-array [-0.5, -0.5, 0,
                               0.5, -0.5, 0,
                               0, 0.5, 0]))
        (.setIndices (short-array [(short 0), (short 1), (short 2)]))))
    (render [delta]
      ;;(.glClearColor (. Gdx gl) 1 0 0 1)
      (.glClear (. Gdx gl) (. GL20 GL_COLOR_BUFFER_BIT))
      (.begin meshShader)
      (.render mesh meshShader (. GL20 GL_TRIANGLES))
      (.end meshShader))
    (dispose[]
      (.dispose mesh)
      (.dispose meshShader))
    (hide [])
    (pause [])
    (resize [w h])
    (resume [])))
