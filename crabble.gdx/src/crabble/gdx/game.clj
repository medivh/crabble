(ns crabble.gdx.game
  (:import (com.badlogic.gdx Game Screen Gdx))
  (:require (crabble.gdx [screen :as screen])
            (crabble.gdx [grid :as grid])
            (crabble.gdx [triangle :as triangle])))

(gen-class
  :name crabble.gdx.game.CrabbleGame
  :extends com.badlogic.gdx.Game)

(defn -create [^Game this]
  ;;(.setScreen this (grid/create-grid)))
  ;;(.setScreen this (triangle/create-triangle)))
  (.setScreen this (screen/create-screen2)))
