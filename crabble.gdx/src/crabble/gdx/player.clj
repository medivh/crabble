(ns crabble.gdx.player
  (:import (com.badlogic.gdx.math Rectangle Vector2))
  )

(def STATE {:IDLE 1
            :WALKING 2
            :JUMPING 3
            :DYING 4
            })

(gen-class
  :name crabble.gdx.player.Player
  :init init
  :constructors {[com.badlogic.gdx.math.Vector2] []}
  :methods [[getPosition [] com.badlogic.gdx.math.Vector2]
            [getBounds [] com.badlogic.gdx.math.Rectangle]
            [setPosition [com.badlogic.gdx.math.Vector2] void]
            [setState [int] void]
            [update [float] void]]
  :state state)

(def SIZE 0.5)
(def SPEED 4.0)

(defn -init [pos]
  (let [bounds (Rectangle.)]
    (set! (.height bounds) SIZE)
    (set! (.width bounds) SIZE)
    [[] (atom {:pos pos
               :bounds bounds
               :state (:IDLE STATE)
               :velocity (Vector2.)
               :facingLeft true
               })
     ]))

(defmacro setfield
  [this key value]
  `(swap! (.state ~this) into {~key ~value}))

(defmacro getfield
  [this key]
  `(@(.state ~this) ~key))

(defn -getBounds [this]
  (getfield this :bounds))

(defn ^String -getPosition [this]
  (getfield this :pos))

(defn -setPosition [this ^com.badlogic.gdx.math.Vector2 loc]
  (setfield this :pos loc))

(defn -setState [this state]
  (setfield this :state state))

(defn -update [this delta]
  (let [position (getfield this :pos)
        velocity (getfield this :velocity)]
    (doto position
      (.scl (.add (.cpy velocity)) delta))))
