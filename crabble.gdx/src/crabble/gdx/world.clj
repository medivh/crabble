(ns crabble.gdx.world
  (:import (com.badlogic.gdx.math Vector2)
           (com.badlogic.gdx.utils Array)
           (crabble.gdx.block Block)
           (crabble.gdx.player Player)
           ))

(gen-class
  :name crabble.gdx.world.CrabbleWorld
  :init init
  :methods [[getBlocks [] com.badlogic.gdx.utils.Array]
            [getPlayer [] crabble.gdx.player.Player]]
  :state state)

(defn -init []
  (let [player (Player. (Vector2. 7 2))
        blocks (Array.)]
    (doseq [i (take 10 (range))]
      (.add blocks (Block. (Vector2. i 0)))
      (.add blocks (Block. (Vector2. i 6)))
      (if (> i 2)
        (.add blocks (Block. (Vector2. i 1))))
      )
    (doto blocks
      (.add (Block. (Vector2. 9 2)))
      (.add (Block. (Vector2. 9 3)))
      (.add (Block. (Vector2. 9 4)))
      (.add (Block. (Vector2. 9 5)))
      (.add (Block. (Vector2. 6 3)))
      (.add (Block. (Vector2. 6 4)))
      (.add (Block. (Vector2. 6 5))))

    [[] (ref {:blocks blocks
              :player player})]
    ))

(defmacro setfield
  [this key value]
  `(swap! (.state ~this) into {~key ~value}))

(defmacro getfield
  [this key]
  `(@(.state ~this) ~key))

(defn ^String -getPosition [this]
  (getfield this :pos))

(defn ^Array -getBlocks [this]
  (getfield this :blocks))

(defn ^Player -getPlayer [this]
  (getfield this :player))
