(ns crabble.gdx.block
  (:import (com.badlogic.gdx.math Rectangle Vector2)
           (java.lang Object)))

(def SIZE 1.0)

(gen-class
  :name crabble.gdx.block.Block
  :init init
  :constructors {[com.badlogic.gdx.math.Vector2] []}
  :methods [[setPosition [com.badlogic.gdx.math.Vector2] void]
            [getPosition [] com.badlogic.gdx.math.Vector2]
            [getBounds [] com.badlogic.gdx.math.Rectangle]]
  :state state
  )

(defn -init [pos]
  (let [bounds (Rectangle.)]
    (set! (.height bounds) SIZE)
    (set! (.width bounds) SIZE)
    [[] (atom {:pos pos
               :bounds bounds
               })
     ]))

(defmacro setfield
  [this key value]
  `(swap! (.state ~this) into {~key ~value}))

(defmacro getfield
  [this key]
  `(@(.state ~this) ~key))

(defn -getBounds [this]
  (getfield this :bounds))

(defn -setPosition [this ^com.badlogic.gdx.math.Vector2 loc]
  (setfield this :pos loc))

(defn ^Vector2 -getPosition [this]
  (getfield this :pos))
