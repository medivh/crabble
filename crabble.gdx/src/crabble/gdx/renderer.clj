(ns crabble.gdx.renderer
  (:import (com.badlogic.gdx Gdx)
           (com.badlogic.gdx.graphics Color GL10 OrthographicCamera Texture)
           (com.badlogic.gdx.graphics.g2d SpriteBatch TextureRegion)
           (com.badlogic.gdx.graphics.glutils ShapeRenderer ShapeRenderer$ShapeType)
           (com.badlogic.gdx.math Rectangle)
           (com.badlogic.gdx.utils Array)
           (crabble.gdx.block Block)
           (crabble.gdx.player Player)
           (crabble.gdx.world CrabbleWorld)
           )
  (:require [crabble.gdx.block :as block]
            [crabble.gdx.player :as player]) ;; for block.SIZE
  )

(def CAMERA_WIDTH 10.0)
(def CAMERA_HEIGHT 7.0)

(gen-class
  :name crabble.gdx.renderer.WorldRenderer
  :init init
  :constructors {[crabble.gdx.world.CrabbleWorld boolean] []}
  :methods [[render [] void]
            [setSize [int int] void]]
  :state state)

(defn -init [world debug]
  (let [cam (OrthographicCamera. CAMERA_WIDTH CAMERA_HEIGHT)
        debugRenderer (ShapeRenderer.)
        batch (SpriteBatch.)
        textures {:player (Texture. (.internal (. Gdx files) "resources/player.png"))
                  :block (Texture. (.internal (. Gdx files) "resources/block.png"))
                  :block2 (Texture. (.internal (. Gdx files) "resources/wood-square.jpg"))
                  }]
    (.set (. cam position) (/ CAMERA_WIDTH 2.0) (/ CAMERA_HEIGHT 2.0) 0)
    (.update cam)
    [[] (atom {:world world
              :cam cam
              :debug debug
              :debugRenderer debugRenderer
              :batch batch
              :textures textures
              :width 0
              :height 0
              :ppuX 0.0
              :ppuY 0.0})
     ]))

(defmacro setfield
  [this key value]
  `(swap! (.state ~this) into {~key ~value}))

(defmacro getfield
  [this key]
  `(@(.state ~this) ~key))

(defn drawIt [this it texture size]
    (let [xpos (.x (.getPosition it))
          ypos (.y (.getPosition it))
          ppuX (getfield this :ppuX)
          ppuY (getfield this :ppuY)
          batch (getfield this :batch)
          region (TextureRegion. texture 0 0 48 48)]
      ;(println batch blockSize region)
      ;(println xpos ypos ppuX ppuY)
      (doto batch
        (.draw region (* xpos ppuX) (* ypos ppuY) 0.0, 0.0 (* size ppuX) (* size ppuY) 1.0 1.0 0.0))
        ;(.draw texture (* xpos ppuX) (* ypos ppuY) 0 0 100 100))
        ;(.draw region (* xpos ppuX) (* ypos ppuY) (* blockSize ppuX) (* blockSize ppuY)))
        ;(.draw region 1.0 1.0 100.0 100.0))
      ))

(defn drawBlocks [this]
  (let [world (getfield this :world)
        blocks (.getBlocks world)
        textures (getfield this :textures)]
  (doseq [block blocks]
    (drawIt this block (:block textures) block/SIZE)
    )))

(defn drawPlayer [this]
  (let [world (getfield this :world)
        player (.getPlayer world)
        textures (getfield this :textures)]
    (drawIt this player (:player textures) player/SIZE)
    ))

(defn drawItDebug [it renderer]
  (let [rect (.getBounds it)
        x1 (+ (.x (.getPosition it)) (.x rect))
        y1 (+ (.y (.getPosition it)) (.y rect))]
    (doto renderer
      (.rect x1 y1 (.width rect) (.height rect))
      )))

(defn drawDebug [this]
  (let [renderer (getfield this :debugRenderer)
        cam (getfield this :cam)
        world (getfield this :world)
        player (.getPlayer world)]
    (doto renderer
      (.setProjectionMatrix (.combined cam))
      (.begin (. ShapeRenderer$ShapeType Rectangle))
      )
    (.setColor renderer (Color. 1.0 0 0 1.0))
    (doseq [b (.getBlocks world)]
      (drawItDebug b renderer))
    (.setColor renderer (Color. 0 1.0 0 1.0))
    (drawItDebug player renderer)
    (.end renderer)
  ))

(defn -render [this]
  (let [batch (getfield this :batch)
        debug (getfield this :debug)]
    (.begin batch)
    (drawBlocks this)
    (drawPlayer this)
    (.end batch)
    (if [debug] (drawDebug this))
    ))

(defn -setSize [this width height]
  (setfield this :width width)
  (setfield this :height height)
  (setfield this :ppuX (/ (* 1.0 width) CAMERA_WIDTH))
  (setfield this :ppuY (/ (* 1.0 height) CAMERA_HEIGHT)))
