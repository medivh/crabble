(ns crabble.gdx.controller
  (:import (java.util HashMap Map)
           (crabble.gdx.player Player)
           (crabble.gdx.world CrabbleWorld))
  (:require [crabble.gdx.player :as player]))

(gen-class
  :name crabble.gdx.controller.CrabbleWorldController
  :init init
  :constructors [[crabble.gdx.world.CrabbleWorld] []]
  :state state
  )

(def KEYS (atom
            {:LEFT false
             :RIGHT false
             :JUMP false
             :FIRE false
             }))

(defn -init [world]
  (let [player (.getPlayer world)]
    [[] (atom {:world world
               :player player})]))

(defmacro setfield
  [this key value]
  `(swap! (.state ~this) into {~key ~value}))

(defmacro getfield
  [this key]
  `(@(.state ~this) ~key))

(defn setKey [key]
  (swap! KEYS into {key true}))

(defn leftPressed []
  (setKey :LEFT true))

(defn rightPressed []
  (setKey :RIGHT true))

(defn jumpPressed []
  (setKey :JUMP true))

(defn firePressed []
  (setKey :FIRE true))

(defn leftReleased []
  (setKey :LEFT false))

(defn rightReleased []
  (setKey :RIGHT false))

(defn jumpReleased []
  (setKey :JUMP false))

(defn fireReleased []
  (setKey :FIRE false))

(defn processInput [keys player]
  (if (:LEFT KEYS)
    (doto player
      (.setFacingLeft true)
      (.setState (:WALKING player/STATE)))
    (set! (.x (.getVelocity player)) (* -1 player/SPEED)))
  (if (:RIGHT KEYS)
    (doto player
      (.setFacingLeft false)
      (.setState (:WALKING player/STATE)))
    (set! (.x (.getVelocity player)) player/SPEED))
  (if (= (:LEFT KEYS) (:RIGHT KEYS))
    ((doto player
      (.setFacingLeft true)
      (.setState (:IDLE player/STATE)))
    (set! (.x (.getAcceleration player)) 0)
    (set! (.x (.getVelocity player)) 0))))

(defn -update [this delta]
  (processInput KEYS (getfield this :player))
  (.update (getfield this :player) delta))
