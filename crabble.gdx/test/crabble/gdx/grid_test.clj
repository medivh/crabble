(ns crabble.gdx.grid-test
  (:import (com.badlogic.gdx Gdx Screen)
           (com.badlogic.gdx.graphics Color GL10)
           (com.badlogic.gdx.graphics.g2d BitmapFont SpriteBatch)
           (com.badlogic.gdx.math Vector2)
           (com.badlogic.gdx.physics.box2d World))
  (:use clojure.test
        crabble.gdx.grid))

(deftest create-grid-test
  (testing "Grid constructor"
           (let [grid (create-grid)]
             (is (= 1 1))
             )))
