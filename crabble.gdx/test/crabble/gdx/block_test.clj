(ns crabble.gdx.block-test
  (:import (crabble.gdx.block Block)
           (com.badlogic.gdx.math Vector2)
           )
  (:use clojure.test)
  )

(deftest constructor-test
  (let [pos (Vector2. 1 5)
        block (Block. pos)]
    (is (= pos (.getPosition block)))))
