(ns crabble.gdx.player-test
  (:import (crabble.gdx.player Player)
           (com.badlogic.gdx.math Vector2)
           )
  (:use clojure.test)
  )

(deftest constructor-test
  (let [pos (Vector2. 1 5)
        player (Player. pos)]
    (is (= pos (.getPosition player)))))
