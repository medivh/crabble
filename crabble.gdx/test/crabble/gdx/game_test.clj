(ns crabble.gdx.game-test
  (:import (crabble.gdx.game CrabbleGame))
  (:use clojure.test))

(deftest constructor-test
  (let [game (CrabbleGame.)]
    (is (not (nil? game)))))
