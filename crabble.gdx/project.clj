(defproject crabble.gdx "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [com.badlogic.gdx/gdx "0.9.8"]
                 [com.badlogic.gdx/gdx-natives "0.9.8"]
                 [com.badlogic.gdx/gdx-backend-lwjgl "0.9.8"]
                 [com.badlogic.gdx/gdx-backend-lwjgl-natives "0.9.8"]
                 [com.badlogic.gdx/natives-linux "0.9.8"]
                 ]
  :main crabble.gdx.core
  :aot [crabble.gdx.block
        crabble.gdx.player
        crabble.gdx.world
        crabble.gdx.renderer
        crabble.gdx.game
        ])
