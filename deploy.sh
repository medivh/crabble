#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

adb install -d -r $DIR/android/bin/Crabble-debug.apk
adb shell am start -a android.intent.action.MAIN -n com.drhops.crabble/.CrabbleActivity
